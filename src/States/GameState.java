package States;
import Graphics.Assets;
import GameObjects.Player;
import math.Vector2D;

import java.awt.*;

public class GameState {
    private Player player;
    public GameState(){
        player = new Player(new Vector2D(100,500),Assets.player);
    }
    public void update(){

    }
    public void draw(Graphics g){
        player.draw(g);
    }
}
